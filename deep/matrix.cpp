#include "matrix.hpp"

void
Matrix::resize(size_t _nr,size_t _nc){
  assert(_nr>0);
  assert(_nc>0);
  if(data!=nullptr) delete[] data;
  nr=_nr;
  nc=_nc;
  data=new double[nc*nr];
}

void
Matrix::view() const{
  for(size_t i=0;i<nr;++i){
    cout<<'['<<get(i,0);
    for(size_t j=1;j<nc;++j){
      cout<<','<<get(i,j);
    }
    cout<<endl;
  }
}
