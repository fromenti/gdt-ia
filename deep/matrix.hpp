#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "vector.hpp"

class Matrix{
public:
  size_t nr,nc;
  double* data;
  Matrix();
  Matrix(size_t nr,size_t nc);
  ~Matrix();
  void resize(size_t nr,size_t nc);
  double& get(size_t i);
  double get(size_t i) const;
  double& get(size_t i,size_t j);
  double get(size_t i,size_t j) const;
  void view() const;
};

inline
Matrix::Matrix(){
  nr=0;
  nc=0;
  data=nullptr;
}

inline
Matrix::Matrix(size_t _nr,size_t _nc){
  assert(_nr>0);
  assert(_nc>0);
  nr=_nr;
  nc=_nc;
  data=new double[nr*nc];
}

inline
Matrix::~Matrix(){
  if(data!=nullptr) delete[] data;
}

inline
double& Matrix::get(size_t i,size_t j){
  assert(i<nr);
  assert(j<nc);
  return data[i*nc+j];
}

inline
double Matrix::get(size_t i,size_t j) const{
  assert(i<nr);
  assert(j<nc);
  return data[i*nc+j];
}

inline
double& Matrix::get(size_t i){
  assert(i<nr*nc);
  return data[i];
}

inline double
Matrix::get(size_t i) const{
  assert(i<nr*nc);
  return data[i];
}
#endif
