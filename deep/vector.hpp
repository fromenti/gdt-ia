#ifndef VECTOR_HPP
#define VECTOR_HPP
#include <iostream>
#include <cassert>
#include <cstring>
#include "math.hpp"

using namespace std;

class Vector{
public:
  size_t n;
  double* data;
  Vector();
  Vector(const Vector& u);
  Vector(size_t n);
  ~Vector();
  const Vector& operator=(const Vector& u);
  void resize(size_t s);
  size_t argmax() const;
  void softmax();
  void clear();
  friend ostream& operator<<(ostream& os,const Vector& v);
};

ostream& operator<<(ostream& os,const Vector&);

inline
Vector::Vector(){
  n=0;
  data=nullptr;
}

inline
Vector::Vector(size_t _n){
  n=_n;
  data=new double[n];
}

inline
Vector::~Vector(){
  if(data!=nullptr){
    delete[] data;
  }
}

#endif
