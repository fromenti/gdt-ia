#include "vector.hpp"
#include "matrix.hpp"

Vector::Vector(const Vector& u){
  n=u.n;
  data=new double[n];
  memcpy(data,u.data,n*sizeof(double));
}

const Vector&
Vector::operator=(const Vector& u){
  assert(n==u.n);
  memcpy(data,u.data,n*sizeof(double));
  return *this;
}

void
Vector::resize(size_t _n){
  if(data!=nullptr){
    delete[] data;
  }
  n=_n;
  data=new double[n];
}

size_t
Vector::argmax() const{
  double m=data[0];
  size_t res=0;
  for(size_t i=1;i<n;++i){
    double t=data[i];
    if(t>m){
      m=t;
      res=i;
    }
  }
  return res;
}

void
Vector::softmax(){
  double s=0;
  for(size_t i=0;i<n;++i){
    s+=Math::exp(data[i]);
  }
  for(size_t i=0;i<n;++i){
    data[i]=Math::exp(data[i])/s;
  }
}

void
Vector::clear(){
  for(size_t i=0;i<n;++i){
    data[i]=0;
  }
}

ostream& operator<<(ostream& os,const Vector& v){
  if(v.n==0) return os<<"[]";
  os<<'['<<v.data[0];
  for(size_t i=1;i<v.n;++i){
    os<<", "<<v.data[i];
  }
  return os<<']';
}
