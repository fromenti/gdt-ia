#include "mnist/mnist.hpp"
#include "network.hpp"

int main(int argc,char** argv){
  Mnist dataset;
  Network network(28*28,30,10);
  //network.init_normal_distribution(0,1);
  network.init_standard();
  network.train(&dataset,20,10,3.0);
}
